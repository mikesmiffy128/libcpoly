/*
 * arc4random() cannot be explicitly enabled, but may be disabled if your system
 * has it and/or you don't need it. Otherwise it is provided if your system
 * does not provide it.
 */
// #define WANT_ARC4RANDOM 0

/*
 * getopt() is normally provided if your system does not already have a
 * BSD-comptible version. You may explictly enable or disable it here.
 */
// #define WANT_GETOPT 0
// #define WANT_GETOPT 1

/*
 * setprogname() and getprogname() are normally provided on systems which do not
 * already have them. You may explicitly enable or disable them here.
 */
// #define WANT_PROGNAME 0
// #define WANT_PROGNAME 1

/*
 * Plan9-based sendfd() and recvfd() are normally provided on all Unix systems.
 * If your system already provides this, or you do not want it, you can disable
 * it here.
 */
// #define WANT_SENDFD 0

/*
 * strchrnul() is normally provided on systems which do not already have it. You
 * may explicitly enable or disable it here.
 */
// #define WANT_STRCHRNUL 0
// #define WANT_STRCHRNUL 1

/*
 * strtonum() is normally provided on systems which do not already have it. You
 * may explicitly enable or disable it here.
 */
// #define WANT_STRTONUM 0
// #define WANT_STRTONUM 1

/*
 * reallocarray() is expected to be implemented already on most systems, but you
 * may explicitly enable it here if you have an older libc (for example musl
 * 1.1).
 */
// #define WANT_REALLOCARRAY 1
