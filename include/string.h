#ifndef _INC_CPOLY_STRING_H
#define _INC_CPOLY_STRING_H

#include_next <string.h>

char *strchrnul(const char *s, int c);

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
