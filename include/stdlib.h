#ifndef _INC_CPOLY_STDLIB_H
#define _INC_CPOLY_STDLIB_H

#include_next <stdlib.h>
#include <stdint.h>

uint32_t arc4random(void);
void arc4random_buf(void *buf, size_t n);
uint32_t arc4random_uniform(uint32_t upper_bound);

long long strtonum(const char *numstr, long long minval, long long maxval,
		const char **errstrp);

void *reallocarray(void *ptr, size_t nmemb, size_t size);

const char *getprogname(void);
void setprogname(const char *s);

extern char **environ;

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
