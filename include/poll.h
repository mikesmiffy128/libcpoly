#ifndef _INC_CPOLY_POLL_H
#define _INC_CPOLY_POLL_H

#include_next <poll.h>
// Unfortunately this pollutes the namespace a bit more than would be ideal...
// suggested solutions welcome!
#include <signal.h>
#include <time.h>

#if defined(__NetBSD__)
// NetBSD stupidly calls ppoll() pollts(), but also qualifies everything with
// `restrict` for no reason. This doesn't matter due to strict aliasing, so here
// we just pass everything straight through. Better than defining a macro.
// TODO: It appears the next stable NetBSD release will add this wrapper for
// compatibility with everything else! Remove this from here once that happens!
static inline int ppoll(struct pollfd *fds, nfds_t nfds,
		const struct timespec *timeout, const sigset_t *sigmask) {
	return pollts(fds, nfds, timeout, sigmask);
}
#elif defined(__linux__)
// Linux doesn't declare this without _GNU_SOURCE, which is totally broken with
// glibc.
int ppoll(struct pollfd *fds, nfds_t nfds, const struct timespec *timeout,
		  const sigset_t *sigmask);
#endif

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
