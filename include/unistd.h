#ifndef _INC_CPOLY_UNISTD_H
#define _INC_CPOLY_UNISTD_H

#include_next <unistd.h>

extern int optreset;

// glibc hides this behind _GNU_SOURCE
int pipe2(int fds[2], int flags);

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
