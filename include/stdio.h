#ifndef _INC_CPOLY_STDIO_H
#define _INC_CPOLY_STDIO_H

#include_next <stdio.h>

// glibc hides this by default even though it is very portable, and _GNU_SOURCE
// breaks standards
int asprintf(char **strp, const char *fmt, ...);

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
