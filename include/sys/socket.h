#ifndef _INC_CPOLY_SYS_SOCKET_H
#define _INC_CPOLY_SYS_SOCKET_H

// illumos has an outdated msghdr, unless _XPG4_2 is defined, but that manages
// to horribly break other headers. the usual solution of manually declaring
// stuff won't work here, we need to actually use the macro. the good news is it
// can easily be set just for socket.h and unset again afterwards

#if defined(__SunOS) || defined(__sun)
#ifndef _XPG4_2
#define _cpoly_undef_xpg42
#define _XPG4_2
#endif
#endif
#include_next <sys/socket.h>
#if defined(__SunOS) || defined(__sun)
#ifdef _cpoly_undef_xpg42 // don't undef if someone else wants it defined
#undef _XPG4_2
#endif
#endif

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
