#ifndef _INC_CPOLY_SYS_TYPES_H
#define _INC_CPOLY_SYS_TYPES_H

// illumos has old-style mmap() functions based on caddr_t (char *), rather than
// void *, unless either _POSIX_C_SOURCE or _XPG4_2 is defined, but both of
// these manage to horribly break other headers. to avoid compiler warnings with
// minimal hassle, we just redefine caddr_t to void * :) (although only on
// illumos since it's an invasive change that relies on internel headers being a
// certain way)

#if defined(__SunOS) || defined(__sun)
#define caddr_t _cpoly_original_caddr_t
#endif
#include_next <sys/types.h>
#if defined(__SunOS) || defined(__sun)
#undef caddr_t
typedef void *caddr_t;
#endif

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
