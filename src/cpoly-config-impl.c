#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Here's some funky trickery to retreive the cpoly header path in a relocatable
// manner, in a way that is also portable to any Unix OS. Basically, the
// cpoly-config "program" is just a shebang line which leads to the full path
// being passed to this program, and that's enough to be able to work out where
// the include directory is.
int main(int argc, char *argv[]) {
	if (argc != 2) return 1;

	char *lastslash = 0, *secondlast = 0;
	for (char *p = argv[1]; *p; ++p) {
		if (*p == '/') {
			secondlast = lastslash;
			lastslash = p;
		}
	}
	if (!secondlast) return 1;
	*lastslash = '\0';
	if (strcmp(secondlast + 1, "bin")) return 1;
	*secondlast = 0;
	fprintf(stdout, "-isystem %s/include/cpoly -D_FILE_OFFSET_BITS=64",
			argv[1]);

	return 0;
}
