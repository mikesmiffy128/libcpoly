#include "../config.h"
#include "platform.h"

/* A minimal POSIX getopt() implementation in ANSI C
 * Christopher Wellons <wellons@nullprogram.com>
 * Small modifications: Michael Smith <mikesmiffy128@gmail.com>
 *
 * This is free and unencumbered software released into the public domain.
 *
 * This implementation supports the convention of resetting the option
 * parser by assigning optind to 0. This resets the internal state
 * appropriately. It also supports the optreset variable from the BSDs for the
 * same purpose.
 *
 * Ref: http://pubs.opengroup.org/onlinepubs/9699919799/functions/getopt.html
 */

// * glibc getopt() is completely wrong on purpose
// * illumos doesn't support optreset
// * BSDs and Musl all support standard getopt + optreset
#if !defined(WANT_GETOPT) && (GLIBC || ILLUMOS) || WANT_GETOPT

#include <ctype.h>
#include <stdio.h>
#include <string.h>

int optind = 1;
int opterr = 1;
int optopt;
int optreset = 0;
static char *optarg;

int getopt(int argc, char *const argv[], const char *optstring)
{
	static int optpos = 1;
	const char *arg;
	(void)argc;

	/* Reset? */
	if (optreset || !optind) {
		optind = 1;
		optpos = 1;
		optreset = 0;
	}

	arg = argv[optind];
	if (arg && strcmp(arg, "--") == 0) {
		optind++;
		return -1;
	} else if (!arg || arg[0] != '-' || !isalnum(arg[1])) {
		return -1;
	} else {
		const char *opt = strchr(optstring, arg[optpos]);
		optopt = arg[optpos];
		if (!opt) {
			if (opterr && *optstring != ':')
				fprintf(stderr, "%s: illegal option: %c\n", argv[0], optopt);
			return '?';
		} else if (opt[1] == ':') {
			if (arg[optpos + 1]) {
				optarg = (char *)arg + optpos + 1;
				optind++;
				optpos = 1;
				return optopt;
			} else if (argv[optind + 1]) {
				optarg = (char *)argv[optind + 1];
				optind += 2;
				optpos = 1;
				return optopt;
			} else {
				if (opterr && *optstring != ':')
					fprintf(stderr,
							"%s: option requires an argument: %c\n",
							argv[0], optopt);
				return *optstring == ':' ? ':' : '?';
			}
		} else {
			if (!arg[++optpos]) {
				optind++;
				optpos = 1;
			}
			return optopt;
		}
	}
}

#endif

// vi: sw=4 ts=4 noet tw=80
