#define BSD 1
#if defined(__OpenBSD__)
	#define	OPENBSD 1
#elif defined(__FreeBSD__)
	#define	FREEBSD 1
#elif defined(__NetBSD__)
	#define	NETBSD 1
#elif defined(__DragonFly__)
	#define	DRAGONFLY 1
#else
	#undef BSD
#endif
#if defined(__linux__)
	#define LINUX 1
	#include <features.h>
	#if __GLIBC__
		#define GLIBC 1
	#endif
#elif defined(__SunOS) || defined(__sun)
	#define ILLUMOS 1
#elif !BSD
	#define UNKNOWN 1
#endif
