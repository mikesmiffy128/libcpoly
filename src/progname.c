#include "../config.h"
#include "platform.h"

#if !defined(WANT_PROGNAME) && LINUX || WANT_PROGNAME

extern const char *__progname;

const char *getprogname(void) {
	return __progname;
}

void setprogname(const char *s) {
	__progname = s;
}

#endif
