// Of the platforms that _don't have an already-working arc4random, only
// Dragonfly doesn't support getentropy() either as a syscall or a userspace
// function, because Dragonfly is bad. Fortunately, it does have a sysctl which
// at least works in theory.
#if !DRAGONFLY

int getentropy(void *buf, size_t buflen);

#else

static int getentropy(void *buf, size_t buflen) {
	if (sysctlbyname("kern.random", buf, &buflen, 0, 0)) {
		errno = EIO;
		return -1;
	}
	return 0;
}

#endif

// vi: sw=4 ts=4 noet tw=80
