#include "../config.h"
#include "platform.h"

#include <errno.h>
#include <sys/socket.h>
#include <sys/uio.h>

#if !defined(WANT_SENDFD) || WANT_SENDFD

// NetBSD still has a buggy CMSG_HDR which relies on this type from sys/types.h
#ifdef __NetBSD__
typedef unsigned char u_char;
#endif

int sendfd(int sockfd, int fd) {
	char buf[1] = {0};
	struct iovec iov = {.iov_base = buf, .iov_len = 1};
	char cms[CMSG_SPACE(sizeof(int))];
	struct msghdr msg = {.msg_iov = &iov, .msg_iovlen = 1,
			.msg_control = cms, .msg_controllen = sizeof(cms)};
	
	struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_len = CMSG_LEN(sizeof(int));
	cmsg->cmsg_level = SOL_SOCKET;
	cmsg->cmsg_type = SCM_RIGHTS;
	*(int *)CMSG_DATA(cmsg) = fd;

	return -(sendmsg(sockfd, &msg, 0) == -1);
}

int recvfd(int sockfd) {
	char buf[1];
	struct iovec iov = {.iov_base = buf, .iov_len = 1};
	char cms[CMSG_SPACE(sizeof(int))];
	struct msghdr msg = {.msg_iov = &iov, .msg_iovlen = 1,
			.msg_control = cms, .msg_controllen = sizeof(cms)};

	int n = recvmsg(sockfd, &msg, 0);
	if (n == -1) {
		return -1;
	}
	if (n == 0) {
		errno = EBADMSG;
		return -1;
	}

	return *(int *)CMSG_DATA(CMSG_FIRSTHDR(&msg));
}

#endif

// vi: sw=4 ts=4 noet tw=80
